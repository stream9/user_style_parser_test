#include <user_style_parser/user_style_parser.hpp>

#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

namespace fs = std::filesystem;

std::string
slurp_file(fs::path const& path)
{
    std::ifstream file { path };
    if (file.fail()) {
        throw std::runtime_error { "open error" };
    }

    std::ostringstream oss;
    oss << file.rdbuf();

    return oss.str();
}

void
print_error(std::string_view const css,
            std::vector<user_style_parser::error_event> const& errors)
{
    if (errors.empty()) return;

    auto line = 1;
    auto err = errors[0].it;

    auto bol = css.begin();
    for (auto it = css.begin(); it != err; ++it) {
        if (*it == '\n') {
            ++line;
            bol = it + 1;
        }
    }

    auto eol = err;
    for (; eol != css.end(); ++eol) {
        if (*eol == '\n') break;
    }

    std::cout << "parse error at line " << line << std::endl
              << std::string_view(bol, eol - bol) << std::endl
              << std::string(err - bol, ' ')
              << "\x1b[91m"
              << "^---"
              << "\x1b[0m"
              << std::endl;
}

template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

int main(int argc, char *argv[])
{
    namespace us = user_style_parser;

    if (argc == 1) {
        std::cout << "Usage: parse_folder <CSS files' directory>\n";
        return 1;
    }

    std::vector<us::error_event> errors;

    auto handler = [&](us::event const& ev) {
        std::visit(overloaded {
            [](us::document_rule_start_event const&) {},
            [](us::document_rule_end_event const&) {},
            [](us::url_function_event const& ev) {
                std::cout << "url function: " << ev.parameter << std::endl;
            },
            [](us::url_prefix_function_event const& ev) {
                std::cout << "url-prefix function: " << ev.parameter << std::endl;
            },
            [](us::domain_function_event const& ev) {
                std::cout << "domain function: " << ev.parameter << std::endl;
            },
            [](us::regexp_function_event const& ev) {
                std::cout << "regexp function: " << ev.parameter << std::endl;
            },
            [](us::rule_body_event const& ev) {
                std::cout << "rule_body: " << ev.body.size() << std::endl;
            },
            [&](us::error_event const& ev) {
                errors.push_back(ev);
            }
        }, ev);

        return true;
    };

    fs::directory_iterator dir { argv[1] };
    for (auto const& entry: dir) {
        if (!entry.is_regular_file()) continue;

        auto const& path = entry.path();
        auto const& css = slurp_file(path);

        std::cout << path.filename() << std::endl;

        if (!user_style_parser::parse(css, handler)) {
            print_error(css, errors);
        }
    }
}
