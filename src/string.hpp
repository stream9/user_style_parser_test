#ifndef STRING_ALGORITHM_HPP
#define STRING_ALGORITHM_HPP

#include <boost/algorithm/string.hpp>
#include <string_view>

inline bool
is_space(char const ch)
{
    static auto const pred = boost::algorithm::is_space();

    return pred(ch);
}

inline std::string_view
ltrim(std::string_view const s)
{
    auto it = s.begin();
    for (; it != s.end(); ++it) {
        if (!is_space(*it)) break;
    }

    auto i = it - s.begin();

    return s.substr(static_cast<size_t>(i));
}

inline std::string_view
rtrim(std::string_view const s)
{
    auto it = s.rbegin();
    for (; it != s.rend(); ++it) {
        if (!is_space(*it)) break;
    }

    auto i = it.base() - s.begin();

    return s.substr(0, static_cast<size_t>(i));
}

inline std::string_view
trim(std::string_view const s)
{
    return ltrim(rtrim(s));
}

inline std::string_view
make_string_view(std::string_view::iterator const begin,
                 std::string_view::iterator const end)
{
    assert(end >= begin);
    using size_type = std::string_view::size_type;

    return { begin, static_cast<size_type>(end - begin) };
}

#endif // STRING_ALGORITHM_HPP
