#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <variant>

#include <boost/iterator/zip_iterator.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>

#include <user_style_parser/parser.hpp>
#include <user_style_parser/ast.hpp>

#include "string.hpp"
#include "data_dir.hpp"

using namespace user_style_parser;
namespace tst = boost::unit_test;
namespace fs = std::filesystem;
namespace json = stream9::json;

using iterator = std::string_view::iterator;

struct function {
    std::string type;
    std::string parameter;
};

struct outcome {
    std::vector<function> functions;
    std::string body;
};

struct error {
    std::string code;
    int position;
};

std::string
slurp_file(fs::path const& path)
{
    std::ifstream ifs { path };
    assert(ifs.is_open());

    std::ostringstream oss;
    oss << ifs.rdbuf();

    return oss.str();
}

std::string
join_lines(json::array const& lines)
{
    std::string result;

    for (auto const& line: lines) {
        result += *line.get_string();
        result += '\n';
    }

    if (result.back() == '\n') {
        result.pop_back();
    }

    return result;
}

void
compare_body(std::string_view const result,
             std::string_view const expectation)
{
    BOOST_CHECK_EQUAL(trim(result), expectation);
}

void
compare_function(ast::function const& result,
                 function const& expectation)
{
    auto const& type = expectation.type;

    if (type == "url") {
        BOOST_TEST(std::holds_alternative<ast::url_function>(result));
    }
    else if (type == "url-prefix") {
        BOOST_TEST(std::holds_alternative<ast::url_prefix_function>(result));
    }
    else if (type == "domain") {
        BOOST_TEST(std::holds_alternative<ast::domain_function>(result));
    }
    else if (type == "regexp") {
        BOOST_TEST(std::holds_alternative<ast::regexp_function>(result));
    }
    else {
        BOOST_TEST(false);
    }

    std::visit(
        [&](auto const& f) {
            BOOST_CHECK_EQUAL(f.parameter, expectation.parameter);
        },
        result
    );
}

template<typename T1, typename T2>
auto
zip(T1 const& v1, T2 const & v2)
{
    return boost::make_iterator_range(
        boost::make_zip_iterator(boost::make_tuple(v1.begin(), v2.begin())),
        boost::make_zip_iterator(boost::make_tuple(v1.end(), v2.end()))
    );
}

void
compare_functions(std::vector<ast::function> const& results,
                  std::vector<function> const& expectations)
{
    BOOST_CHECK_EQUAL(results.size(), expectations.size());

    for (auto const& item: zip(results, expectations)) {
        auto const& result = boost::get<0>(item);
        auto const& expectation = boost::get<1>(item);

        compare_function(result, expectation);
    }
}

static void
compare_error_code(parse_errc const result,
                   std::string_view const expectation)
{
    BOOST_CHECK_EQUAL(to_string(result), expectation);
}

void
compare_error_position(std::string_view const input,
                       iterator const it,
                       double const expectation)
{
    auto const pos = it - input.begin();

    BOOST_CHECK_MESSAGE(static_cast<double>(pos) == expectation,
                        "[" << pos << " != " << expectation << "]\n"
                            << error_position(input, it));
}

void
check_success(std::vector<ast::document_rule> const& rules,
              std::vector<outcome> const& outcomes)
{
    BOOST_REQUIRE_EQUAL(rules.size(), outcomes.size());

    for (auto const& item: zip(rules, outcomes)) {
        auto const& rule = boost::get<0>(item);
        auto const& expectation = boost::get<1>(item);

        compare_functions(rule.functions, expectation.functions);
        compare_body(rule.body, expectation.body);
    }
}

void
check_failure(std::string_view const input,
              std::vector<user_style_parser::error_event> const& results,
              std::vector<error> const& errors)
{
    BOOST_REQUIRE_EQUAL(errors.size(), results.size());

    for (auto const& item: zip(results, errors)) {
        auto const& result = boost::get<0>(item);
        auto const& expectation = boost::get<1>(item);

        compare_error_code(result.code, expectation.code);
        compare_error_position(input, result.it, expectation.position);
    }
}

void
test_main(std::string_view const input,
          std::vector<outcome> expected_outcomes,
          std::vector<error> expected_errors)
{
    std::vector<ast::document_rule> rules;
    std::vector<error_event> errors;
    ast::builder builder { rules, errors };

    auto handler = [&](auto const& ev) {
        std::visit(builder, ev);
        return true;
    };

    auto const ok = parse(input, handler);

    if (ok) {
        check_success(rules, expected_outcomes);
    }
    else {
        check_failure(input, errors, expected_errors);
    }
}

static void
read_outcome(std::vector<struct outcome>& result, json::array const& arr)
{
    for (auto const& v: arr) {
        assert(v.is_object());
        auto const& obj = *v.get_object();

        struct outcome outcome;

        auto const& functions_v = obj.at("functions");
        assert(functions_v.is_array());
        for (auto const& v: *functions_v.get_array()) {
            assert(v.is_object());
            auto const& obj = *v.get_object();

            auto const& type_v = obj.at("type");
            assert(type_v.is_string());
            auto const& param_v = obj.at("parameter");
            assert(param_v.is_string());

            outcome.functions.emplace_back(
                std::string(*type_v.get_string()),
                std::string(*param_v.get_string()) );
        }

        auto const& body_v = obj.at("body");
        assert(body_v.is_string());

        outcome.body = std::string(*body_v.get_string());

        result.push_back(std::move(outcome));
    }
}

static void
read_error(std::vector<error>& result, json::array const& arr)
{
    for (auto const& v: arr) {
        assert(v.is_object());
        auto const& obj = *v.get_object();

        auto const& code_v = obj.at("code");
        assert(code_v.is_string());
        auto const& position_v = obj.at("position");
        assert(position_v.is_number());

        result.emplace_back(
            std::string(*code_v.get_string()),
            static_cast<int>(*position_v.get_number())
        );
    }
}

void
make_test_case(tst::test_suite& parent, fs::path const& path)
{
    auto* const suite = BOOST_TEST_SUITE(path.stem().c_str());
    assert(suite);

    auto const& test_data = slurp_file(path);

    auto const& value = json::parse(test_data);
    assert(value.is_array());
    auto const& array = *value.get_array();

    for (auto const& v: array) {
        assert(v.is_object());
        auto const& obj = *v.get_object();

        auto const& name = std::string(*obj.at("name").get_string());
        auto const& input = join_lines(*obj.at("input").get_array());

        std::vector<outcome> outcomes;
        std::vector<error> errors;

        auto it = obj.find("outcome");
        if (it != obj.end()) {
            assert(it->value.is_array());
            read_outcome(outcomes, *it->value.get_array());
        }

        it = obj.find("error");
        if (it != obj.end()) {
            assert(it->value.is_array());
            read_error(errors, *it->value.get_array());
        }

        auto case_ = new tst::test_case(
            name.data(),
            __FILE__, __LINE__,
            [input, outcomes, errors]() {
                test_main(input, outcomes, errors);
            }
        );

        suite->add(case_);
    }

    parent.add(suite);
}

void
read_data_directory(tst::test_suite& parent, fs::path const path)
{
    fs::directory_iterator dir {
        path,
        fs::directory_options::skip_permission_denied
    };

    for (auto const& entry: dir) {
        auto const& path = entry.path();

        if (entry.is_directory()) {
            auto* const suite = BOOST_TEST_SUITE(path.stem().c_str());
            assert(suite);

            read_data_directory(*suite, path);

            parent.add(suite);

            continue;
        }
        else if (entry.is_regular_file()) {
            if (path.extension() != ".json") continue;

            make_test_case(parent, path);
        }
    }
}

bool
init_unit_test()
{
    read_data_directory(
            tst::framework::master_test_suite(), testing::data_dir());

    return true;
}

int main(int argc, char* argv[])
{
    return tst::unit_test_main(&init_unit_test, argc, argv);
}
